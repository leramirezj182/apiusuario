const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('./cors');
const Usuarios = require('../models/Usuario');

const UsuarioRouter = express.Router();
UsuarioRouter.use(bodyParser.json());

UsuarioRouter.route('/').
options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, (req, res, next) => {
        Usuarios.find({}).sort({Nombre: -1})
            .then((usuarios) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(usuarios);
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .post(cors.corsWithOptions,
        (req, res, next) => {
        Usuarios.create(req.body)
            .then((usuario) => {
                console.log('Usuario Created', usuario);
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(usuario);

            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .put(cors.corsWithOptions, 
        (req, res, next) => {
        res.statusCode = 403;
        res.end('La operacion PUT no es soportada para esta ruta');
    })
    .delete(cors.corsWithOptions, 
        (req, res, next) => {
        res.statusCode = 403;
        res.end('La operacion DELETE no es soportada para esta ruta');
    });

UsuarioRouter.route('/:usuarioId').
options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, (req, res, next) => {
        Usuarios.findById(req.params.usuarioId)
            .then((usuarios) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(usuarios);
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .post(cors.corsWithOptions, 
        (req, res, next) => {

    })
    .put(cors.corsWithOptions, 
        (req, res, next) => {
        Usuarios.findByIdAndUpdate(req.params.usuarioId, {
            $set: req.body
        }, { new: true })
            .then((usuario) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(usuario);
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .delete(cors.corsWithOptions, 
        (req, res, next) => {
        Usuarios.findByIdAndRemove(req.params.usuarioId)
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(resp);
            }, (err) => next(err))
            .catch((err) => next(err));
    });



module.exports = UsuarioRouter;
