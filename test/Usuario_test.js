var superagent = require('superagent')
var expect = require('expect.js')

describe('Test Usuario', function () {
  var id
  var url = 'http://localhost:3000';


  // ------------------Pruebas routes Usuario------------------

  // POST / Usuario
  it('Test POST /Usuario ', function (done) {
    superagent.post(url + '/Usuario')
      .send({
        Nombres:"Luis Eduardo",
        Apellidos:"Ramirez Jerez",
        Codigo:"20182195004",
        Email:"leramirezj@correo.udistrital.edu.co",
        Estado:"activo"
      })
      .end(function (e, res) {
        id = res.body._id;
       
        expect(e).to.eql(null)
        //expect(res.body.status).to.eql('created')
        expect(res.body._id).to.be.an('string')
        done()
      })
  })
  // GET /Usuario
  it('Test GET /Usuario ', function (done) {
    superagent.get(url + '/Usuario')
      .end(function (e, res) {

        expect(e).to.eql(null)
        expect(typeof res.body).to.eql('object')
        done()
      })
  })

 // GET /Usuario/:id
 it('test GET /Usuario/:id ', function (done) {
  superagent.get(url + '/Usuario/'+id)
        .end(function (e, res) {
         //expect(e).to.eql(null)
        expect(typeof res.body).to.eql('object')
        expect(res.body._id).to.eql(id)
        done()
    })
})
// PUT /Usuario/:id
it('Test PUT /Usuario/:id ', function (done) {
  console.log("el id es: "+id)
  superagent.put(url + '/Usuario/'+id)
    .send({
        Nombres: "Luis Eduardo",
        Apellidos: "Ramirez Jerez",
        Codigo: "20182195004",
        Email: "leramirezj@correo.udistrital.edu.co",
        Estado:"Inactivo"
    })
    .end(function (e, res) {
      //expect(e).to.eql(null)
      //expect(res.body.status).to.eql('updated')
      console.log(res.body)
      expect(res.body.Estado).to.eql('Inactivo')
      done()
    })
})

// DELETE /Usuario/:id
it('Test DELETE /Usuario/:id ', function (done) {
  superagent.delete(url +'/Usuario/'+id)
    .end(function (e, res) {
      //expect(e).to.eql(null)
      expect(typeof res.body).to.eql('object')
      //expect(res.body.status).to.eql('deleted')
      done()
    })
})

})