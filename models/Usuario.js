const mongoose = require('mongoose');
const Schemma = mongoose.Schema;


const UsuarioSchema = new Schemma({
    
    Nombres: {
        type: String,
        required: true
    },
    Apellidos: {
        type: String,
        required: true
    },
    Codigo: {
        type: String,
        required: true
    },
    Email: {
        type: String,
        required: true
    },
    Estado: {
        type: String,
        required: true
    },
    
}, {
        timestamps: false
    });


var Usuarios = mongoose.model('Usuario', UsuarioSchema);

module.exports = Usuarios;
